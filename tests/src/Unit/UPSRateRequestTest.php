<?php

namespace Drupal\Tests\commerce_ups\Unit;

use Drupal\commerce_price\Entity\CurrencyInterface;
use Drupal\commerce_price\Rounder;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_ups\UPSRateRequest;
use Drupal\commerce_ups\UPSRateRequestInterface;
use Drupal\commerce_ups\UPSShipment;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\physical\LengthUnit;
use Drupal\physical\WeightUnit;

/**
 * Tests the UPS rate request.
 *
 * @coversDefaultClass \Drupal\commerce_ups\UPSRateRequest
 * @group commerce_ups
 */
class UPSRateRequestTest extends UPSUnitTestBase {

  /**
   * A UPS rate request object.
   *
   * @var \Drupal\commerce_ups\UPSRateRequest
   */
  protected $rateRequest;

  /**
   * The rounder.
   *
   * @var \Drupal\commerce_price\Rounder
   */
  protected $rounder;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Set up requirements for test.
   */
  protected function setUp() : void {
    parent::setUp();

    $logger_factory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $logger = $this->prophesize(LoggerChannelInterface::class);
    $logger_factory->get(UPSRateRequestInterface::LOGGER_CHANNEL)
      ->willReturn($logger->reveal());

    $usd_currency = $this->prophesize(CurrencyInterface::class);
    $usd_currency->id()->willReturn('USD');
    $usd_currency->getFractionDigits()->willReturn('2');

    $eur_currency = $this->prophesize(CurrencyInterface::class);
    $eur_currency->id()->willReturn('EUR');
    $eur_currency->getFractionDigits()->willReturn('2');

    $storage = $this->prophesize(EntityStorageInterface::class);
    $storage->load('USD')->willReturn($usd_currency->reveal());
    $storage->load('EUR')->willReturn($eur_currency->reveal());

    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $entity_type_manager->getStorage('commerce_currency')
      ->willReturn($storage->reveal());

    $this->rounder = new Rounder($entity_type_manager->reveal());
    $this->cacheBackend = $this->prophesize(CacheBackendInterface::class);
    $this->rateRequest = new UPSRateRequest(new UPSShipment(), $logger_factory->reveal(), $this->rounder, $this->cacheBackend->reveal());
    $this->rateRequest->setConfig($this->configuration);
  }

  /**
   * Test getAuth response.
   *
   * @covers ::getAuth
   */
  public function testAuth() {
    $auth = $this->rateRequest->getAuth();

    $this->assertEquals($auth['access_key'], $this->configuration['api_information']['access_key']);
    $this->assertEquals($auth['user_id'], $this->configuration['api_information']['user_id']);
    $this->assertEquals($auth['password'], $this->configuration['api_information']['password']);
  }

  /**
   * Test useIntegrationMode().
   *
   * @covers ::useIntegrationMode
   */
  public function testIntegrationMode() {
    $mode = $this->rateRequest->useIntegrationMode();

    $this->assertEquals(TRUE, $mode);
  }

  /**
   * Test getRateType().
   *
   * @covers ::getRateType
   */
  public function testRateType() {
    $type = $this->rateRequest->getRateType();

    $this->assertEquals(TRUE, $type);
  }

  /**
   * Test rate requests return valid rates.
   *
   * @param string $weight_unit
   *   Weight unit.
   * @param string $length_unit
   *   Length unit.
   * @param bool $send_from_usa
   *   Whether the shipment should be sent from USA.
   *
   * @covers ::getRates
   *
   * @dataProvider measurementUnitsDataProvider
   */
  public function testRateRequest($weight_unit, $length_unit, $send_from_usa) {
    // Invoke the rate request object.
    $shipment = $this->mockShipment($weight_unit, $length_unit, $send_from_usa);
    $shipping_method_plugin = $this->mockShippingMethod();
    $shipping_method_entity = $this->prophesize(ShippingMethodInterface::class);
    $shipping_method_entity->id()->willReturn('123456789');
    $shipping_method_entity->getPlugin()->willReturn($shipping_method_plugin);
    $rates = $this->rateRequest->getRates($shipment, $shipping_method_entity->reveal());

    // Make sure at least one rate was returned.
    $this->assertArrayHasKey(0, $rates);

    foreach ($rates as $rate) {
      assert($rate instanceof ShippingRate);
      $this->assertGreaterThan(0, $rate->getAmount()->getNumber());
      $this->assertGreaterThan(0, $rate->getOriginalAmount()->getNumber());
      $this->assertEquals($rate->getAmount()->getCurrencyCode(), $send_from_usa ? 'USD' : 'EUR');
      $this->assertNotEmpty($rate->getService()->getLabel());
      $this->assertEquals('123456789', $rate->getShippingMethodId());
    }
  }

  /**
   * Data provider for testRateRequest()
   */
  public function measurementUnitsDataProvider() {
    $weight_units = [
      WeightUnit::GRAM,
      WeightUnit::KILOGRAM,
      WeightUnit::OUNCE,
      WeightUnit::POUND,
    ];
    $length_units = [
      LengthUnit::MILLIMETER,
      LengthUnit::CENTIMETER,
      LengthUnit::METER,
      LengthUnit::INCH,
      LengthUnit::FOOT,
    ];
    foreach ($weight_units as $weight_unit) {
      foreach ($length_units as $length_unit) {
        yield [$weight_unit, $length_unit, TRUE];
        yield [$weight_unit, $length_unit, FALSE];
      }
    }
  }

}
