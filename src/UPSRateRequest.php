<?php

namespace Drupal\commerce_ups;

use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Ups\Entity\RateInformation;
use Ups\Entity\Shipment;
use Ups\Rate;

/**
 * Represents the UPS rate request.
 */
class UPSRateRequest extends UPSRequest implements UPSRateRequestInterface {

  /**
   * How long the rate request will be cached.
   *
   * We might want to make this configurable in the future.
   */
  public const RATE_CACHE_DURATION = 3600;

  /**
   * A shipping method configuration array.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The UPS Shipment object.
   *
   * @var \Drupal\commerce_ups\UPSShipmentInterface
   */
  protected $upsShipment;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Constructs a new UPSRateRequest object.
   *
   * @param \Drupal\commerce_ups\UPSShipmentInterface $ups_shipment
   *   The UPS shipment object.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   The price rounder.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   */
  public function __construct(UPSShipmentInterface $ups_shipment, LoggerChannelFactoryInterface $logger_factory, RounderInterface $rounder, CacheBackendInterface $cache_backend) {
    $this->upsShipment = $ups_shipment;
    $this->logger = $logger_factory->get(UPSRateRequestInterface::LOGGER_CHANNEL);
    $this->rounder = $rounder;
    $this->cacheBackend = $cache_backend;
  }

  /**
   * Logs Requests and responses from UPS.
   *
   * @param string $message
   *   The message to log.
   * @param mixed $data
   *   The UPS Request or response object.
   */
  protected function log(string $message, $data) {
    $this->logger->info("$message <br><pre>@rate_request</pre>", [
      '@rate_request' => var_export($data, TRUE),
    ]);
  }

  /**
   * Fetch rates from the UPS API.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The commerce shipment.
   * @param \Drupal\commerce_shipping\Entity\ShippingMethodInterface $shipping_method
   *   The shipping method.
   *
   * @throws \Exception
   *   Exception when required properties are missing.
   *
   * @return array
   *   An array of ShippingRate objects.
   */
  public function getRates(ShipmentInterface $commerce_shipment, ShippingMethodInterface $shipping_method) {
    $rates = [];

    try {
      $auth = $this->getAuth();
    }
    catch (\Exception $e) {
      $this->logger->error(
        new TranslatableMarkup(
          'Unable to fetch authentication config for UPS. Please check your shipping method configuration.'
        )
      );
      return [];
    }

    $request = new Rate(
      $auth['access_key'],
      $auth['user_id'],
      $auth['password'],
      $this->useIntegrationMode()
    );

    try {
      $shipment = $this->upsShipment->getShipment($commerce_shipment, $shipping_method->getPlugin());

      // Enable negotiated rates, if enabled.
      if ($this->getRateType()) {
        $rate_information = new RateInformation();
        $rate_information->setNegotiatedRatesIndicator(TRUE);
        $rate_information->setRateChartIndicator(FALSE);
        $shipment->setRateInformation($rate_information);
      }

      $rate_request_cache_key = 'rate-request-' . hash('sha256', $this->buildRateRequestKey($shipment));
      $rate_request_cache = $this->cacheBackend->get($rate_request_cache_key);
      if (!empty($rate_request_cache)) {
        $ups_rates = $rate_request_cache->data;
      }
      else {
        // Shop Rates.
        if (!empty($this->configuration['options']['log']['request'])) {
          $this->log("Sending UPS Request.", $shipment);
        }
        $ups_rates = $request->shopRates($shipment);
        if (!empty($this->configuration['options']['log']['response'])) {
          $this->log("UPS response received.", $ups_rates);
        }
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      $ups_rates = [];
    }

    if (!empty($ups_rates->RatedShipment)) {

      foreach ($ups_rates->RatedShipment as $ups_rate) {
        $service_code = $ups_rate->Service->getCode();

        // Only add the rate if this service is enabled.
        if (!in_array($service_code, $this->configuration['services'])) {
          continue;
        }

        // Use negotiated rates if they were returned.
        if ($this->getRateType() && !empty($ups_rate->NegotiatedRates->NetSummaryCharges->GrandTotal->MonetaryValue)) {
          $cost = $ups_rate->NegotiatedRates->NetSummaryCharges->GrandTotal->MonetaryValue;
          $currency = $ups_rate->NegotiatedRates->NetSummaryCharges->GrandTotal->CurrencyCode;
        }
        // Otherwise, use the default rates.
        else {
          $cost = $ups_rate->TotalCharges->MonetaryValue;
          $currency = $ups_rate->TotalCharges->CurrencyCode;
        }

        $price = new Price((string) $cost, $currency);
        $service_name = $ups_rate->Service->getName();

        $multiplier = (!empty($this->configuration['rate_options']['rate_multiplier']))
          ? $this->configuration['rate_options']['rate_multiplier']
          : 1.0;
        if ($multiplier != 1) {
          $price = $price->multiply((string) $multiplier);
        }

        $round = !empty($this->configuration['options']['round'])
          ? $this->configuration['options']['round']
          : PHP_ROUND_HALF_UP;
        $price = $this->rounder->round($price, $round);
        $rates[] = new ShippingRate([
          'shipping_method_id' => $shipping_method->id(),
          'service' => new ShippingService($service_code, $service_name),
          'amount' => $price,
        ]);
      }
      $this->cacheBackend->set($rate_request_cache_key, $ups_rates, time() + self::RATE_CACHE_DURATION);
    }
    return $rates;
  }

  /**
   * Gets the rate type: whether we will use negotiated rates or standard rates.
   *
   * @return bool
   *   Returns true if negotiated rates should be requested.
   */
  public function getRateType() {
    return boolval($this->configuration['rate_options']['rate_type']);
  }

  /**
   * Build the Rate request key.
   *
   * Note: this is reflects the elements of UPSShipment that are currently
   * utilized when checking rates. If UPSShipment->getShipment() is enhanced,
   * this method should reflect those changes.
   *
   * @param \Ups\Entity\Shipment $shipment
   *   The shipment.
   *
   * @return string
   *   The xml for the request.
   *
   * @throws \DOMException
   */
  private function buildRateRequestKey(Shipment $shipment): string {
    $document = $xml = new \DOMDocument();
    $xml->formatOutput = TRUE;

    $document->appendChild($shipment->getShipFrom()->toNode($document));
    $document->appendChild($shipment->getShipTo()->toNode($document));
    foreach ($shipment->getPackages() as $package) {
      $document->appendChild($package->toNode($document));
    }
    $document->appendChild($shipment->getShipFrom()->toNode($document));
    if ($this->getRateType() && $shipment->getRateInformation()) {
      $document->appendChild($shipment->getRateInformation()
        ->toNode($document));
    }
    return $xml->saveXML();
  }

}
